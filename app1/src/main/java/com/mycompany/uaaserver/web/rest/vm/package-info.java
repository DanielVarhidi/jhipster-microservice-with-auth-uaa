/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mycompany.uaaserver.web.rest.vm;
